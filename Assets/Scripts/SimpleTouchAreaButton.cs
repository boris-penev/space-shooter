﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SimpleTouchAreaButton : MonoBehaviour, IPointerDownHandler,
IPointerUpHandler
{
    private bool canFire, touched;
    private int pointerId;

    void Awake()
    {
        touched = false;
    }

    public void OnPointerDown(PointerEventData data)
    {
        if (!touched)
        {
            canFire = true;
            touched = true;
            pointerId = data.pointerId;
        }
    }

    public void OnPointerUp(PointerEventData data)
    {
        if (data.pointerId == pointerId)
        {
            canFire = false;
            touched = false;
        }
    }

    public bool CanFire()
    {
        return canFire;
    }
}
