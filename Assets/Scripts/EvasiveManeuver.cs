﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvasiveManeuver : MonoBehaviour
{
    public float sidewaysVelocity, tilt;
    public Boundary boundary;
    public Vector2 maneuverTime, maneuverWait, startWait;
    private float currentSpeed, targetManeuver;
    private Rigidbody rigidBody;
    private Transform playerTransform;

    // Use this for initialization
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        currentSpeed = rigidBody.velocity.z;
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player == null)
        {
            return;
        }
        playerTransform = player.transform;
        if (playerTransform == null)
        {
            return;
        }
        StartCoroutine(Evade());
    }
	
    void FixedUpdate()
    {
        float newManeuver =
            Mathf.MoveTowards(rigidBody.velocity.x, targetManeuver,
                Time.deltaTime * Random.Range(0, sidewaysVelocity));
        rigidBody.velocity = new Vector3(newManeuver, 0.0f, currentSpeed);
        rigidBody.position = new Vector3(
            Mathf.Clamp(rigidBody.position.x, boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp(rigidBody.position.z, boundary.zMin, boundary.zMax)
        );
        rigidBody.rotation = Quaternion.Euler(0.0f, 0.0f,
            rigidBody.velocity.x * -tilt);
    }

    IEnumerator Evade()
    {
        yield return new WaitForSeconds(Random.Range(startWait.x, startWait.y));
        while (playerTransform != null)
        {
            targetManeuver = playerTransform.position.x;
            yield return new WaitForSeconds(
                    Random.Range(maneuverTime.x, maneuverTime.y));
            targetManeuver = 0;
            yield return new WaitForSeconds(
                    Random.Range(maneuverWait.x, maneuverWait.y));
        }
    }
}
