﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public float boltSpeed, fireDelta, startDelay;
    public GameObject shot;
    public Transform shotSpawn;
    private AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        InvokeRepeating("Fire", startDelay, fireDelta);
    }

    void Fire()
    {
        GameObject boltInstance = Instantiate(shot, shotSpawn.position,
            shotSpawn.rotation);
        Mover mv = boltInstance.GetComponent<Mover>();
        mv.velocity.x = Mathf.Max(mv.velocity.x, boltSpeed);
        audioSource.Play();
    }
}
