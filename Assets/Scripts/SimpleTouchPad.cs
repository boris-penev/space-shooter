﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SimpleTouchPad : MonoBehaviour, IPointerDownHandler, IDragHandler,
IPointerUpHandler
{
    private bool touched;
    public float acceleration, braking, directionMagnitude;
    private int pointerId;
    private Vector2 direction, origin, smoothDirection;

    void Awake()
    {
        touched = false;
        direction = Vector2.zero;
        smoothDirection = Vector2.zero;
    }

    public void OnPointerDown(PointerEventData data)
    {
        if (!touched)
        {
            touched = true;
            pointerId = data.pointerId;
            origin = data.position;
        }
    }

    public void OnDrag(PointerEventData data)
    {
        if (data.pointerId == pointerId)
        {
            Vector2 directionRaw = data.position - origin;
            direction = directionMagnitude * directionRaw.normalized;
        }
    }

    public void OnPointerUp(PointerEventData data)
    {
        if (data.pointerId == pointerId)
        {
            touched = false;
            direction = Vector2.zero;
        }
    }

    public Vector2 GetDirection()
    {
        if (touched)
        {
            smoothDirection = Vector2.MoveTowards(smoothDirection, direction,
                acceleration);
        }
        else
        {
            smoothDirection = Vector2.MoveTowards(smoothDirection, direction,
                braking);
        }
        return smoothDirection;
    }
}
