﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeImage : MonoBehaviour
{
    public float fadeDuration, targetAlpha;
    public Image image;

    // Use this for initialization
    void Start()
    {
        image.CrossFadeAlpha(targetAlpha, fadeDuration, false);
    }
}
