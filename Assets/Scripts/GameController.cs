﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public float spawnWait, startWait, beforeWaveWait, afterWaveWait,
        hazardSidewaysSpeed;
    public int hazardCount;
    public GameObject background, restartButton, starField, starFieldDistant;
    public GameObject[] asteroids, enemyShips;
    public Image fireZone, movementZone;
    public Text fireText, gameOverText, movementText, restartText, scoreText, waveText;
    public Vector2 scaleNumberOfAsteroids, scaleNumberOfEnemyShips,
        scaleBackgroundScroller, scaleSpawnWait, scaleHazardSpeed,
        scaleStartDelay, scaleShot, scaleSidewaysSpeed, scaleManeuverTime,
        scaleManeuverWait, scaleStartWait;
    public Vector3 spawnValues;
    private bool gameOver, restart;
    private float starFieldInitialSimulationSpeed,
        starFieldDistantInitialSimulationSpeed;
    private int score, wave;
    private BackgroundScroller backgroundScroller;
    private ParticleSystem.MainModule starFieldParticles,
        starFieldDistantParticles;

    public void AddScore(int newScoreValue)
    {
        score += newScoreValue;
        UpdateScore();
    }

    public void GameOver()
    {
        gameOver = true;
        gameOverText.enabled = true;
        waveText.enabled = true;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);﻿ 
    }

    // Use this for initialization
    void Start()
    {
        score = 0;
        gameOver = false;
        restart = false;
        restartButton.SetActive(false);
        restartText.enabled = false;
        gameOverText.enabled = false;
        fireText.enabled = true;
        movementText.enabled = true;
        waveText.text = "";
        waveText.enabled = false;
        #if UNITY_ANDROID
        fireZone.enabled = true;
        movementZone.enabled = true;
        #else
        fireZone.enabled = false;
        movementZone.enabled = false;
        #endif
        backgroundScroller = background.GetComponent<BackgroundScroller>();
        starFieldParticles = starField.GetComponent<ParticleSystem>().main;
        starFieldDistantParticles =
            starFieldDistant.GetComponent<ParticleSystem>().main;
        starFieldInitialSimulationSpeed = starFieldParticles.simulationSpeed;
        starFieldDistantInitialSimulationSpeed =
            starFieldDistantParticles.simulationSpeed;
        UpdateScore();
        StartCoroutine(SpawnWaves());
    }

    // Update is called once per frame
    void Update()
    {
        #if !UNITY_ANDROID
        if (restart && Input.GetKeyDown(KeyCode.R))
        {
            RestartGame();﻿ 
        }
        #endif
    }

    IEnumerator SpawnWaves()
    {
        yield return new WaitForSeconds(startWait);
        wave = 0;
        while (!gameOver)
        {
            yield return StartCoroutine(SpawnWave(++wave));
        }
        CheckIfGameOver();
    }

    IEnumerator SpawnWave(int wave)
    {
        int numberOfAsteroids = (int)(scaleNumberOfAsteroids.x * wave
                                + scaleNumberOfAsteroids.y);
        int numberOfEnemyShips = (int)(scaleNumberOfEnemyShips.x * wave
                                 + scaleNumberOfEnemyShips.y);
        waveText.text = "Wave " + wave;
        waveText.enabled = true;
        yield return new WaitForSeconds(beforeWaveWait);
        waveText.enabled = false;
        backgroundScroller.targetSpeed = backgroundScroller.initialSpeed *
        (scaleBackgroundScroller.x * wave + scaleBackgroundScroller.y);
        starFieldParticles.simulationSpeed = starFieldInitialSimulationSpeed *
            (scaleBackgroundScroller.x * wave + scaleBackgroundScroller.y);
        starFieldDistantParticles.simulationSpeed =
            starFieldDistantInitialSimulationSpeed *
            (scaleBackgroundScroller.x * wave + scaleBackgroundScroller.y);
        float currentSpawnWait = spawnWait / (scaleSpawnWait.x * wave
                                 + scaleSpawnWait.y);
        int asteroidsSpawned = 0, enemyShipsSpawned = 0;
        while (asteroidsSpawned < numberOfAsteroids
               || enemyShipsSpawned < numberOfEnemyShips)
        {
            bool isShip = false;
            GameObject hazard = chooseHazard(ref asteroidsSpawned,
                                    numberOfAsteroids, ref enemyShipsSpawned,
                                    numberOfEnemyShips, ref isShip);
            Vector3 spawnPosition = new Vector3(
                                        Random.Range(-spawnValues.x, spawnValues.x),
                                        spawnValues.y, spawnValues.z);
            Quaternion spawnRotation = Quaternion.identity;
            GameObject hazardInstance = Instantiate(hazard, spawnPosition,
                                            spawnRotation) as GameObject;
            Mover mv = hazardInstance.GetComponent<Mover>();
            mv.velocity.x *= scaleHazardSpeed.x * wave + scaleHazardSpeed.y;
            mv.velocity.y = Random.Range(-hazardSidewaysSpeed,
                hazardSidewaysSpeed);
            if (isShip)
            {
                WeaponController gun = hazardInstance.
                    GetComponent<WeaponController>();
                gun.startDelay /= scaleStartDelay.x * wave + scaleStartDelay.y;
                gun.boltSpeed = Mathf.Max(gun.boltSpeed,
                    scaleShot.x * wave + scaleShot.y);
                EvasiveManeuver em = hazardInstance.
                    GetComponent<EvasiveManeuver>();
                em.sidewaysVelocity *= scaleSidewaysSpeed.x * wave
                + scaleSidewaysSpeed.y;
                em.maneuverTime /= scaleManeuverTime.x * wave
                + scaleManeuverTime.y;
                em.maneuverWait /= scaleManeuverWait.x * wave
                + scaleManeuverWait.y;
                em.startWait /= scaleStartWait.x * wave + scaleStartWait.y;
            }
            CheckIfGameOver();
            yield return new WaitForSeconds(currentSpawnWait);
        }
        CheckIfGameOver();
        yield return new WaitForSeconds(afterWaveWait);
    }

    GameObject chooseHazard(ref int asteroidsSpawned, int numberOfAsteroids,
                            ref int enemyShipsSpawned, int numberOfEnemyShips,
                            ref bool isShip)
    {
        GameObject hazard = null;
        isShip = false;
        if (asteroidsSpawned < numberOfAsteroids
            && enemyShipsSpawned < numberOfEnemyShips)
        {
            int id = Random.Range(0, asteroids.Length + enemyShips.Length);
            if (id < asteroids.Length)
            {
                ++asteroidsSpawned;
                hazard = asteroids[id];
            }
            else
            {
                ++enemyShipsSpawned;
                isShip = true;
                hazard = enemyShips[id - asteroids.Length];
            }
        }
        else if (asteroidsSpawned < numberOfAsteroids)
        {
            ++asteroidsSpawned;
            hazard = asteroids[Random.Range(0, asteroids.Length)];
        }
        else if (enemyShipsSpawned < numberOfEnemyShips)
        {
            ++enemyShipsSpawned;
            isShip = true;
            hazard = enemyShips[Random.Range(0, enemyShips.Length)];
        }
        else
        {
            Debug.Log("Invalid state  of spawn wave.");
        }
        return hazard;
    }

    void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }

    void CheckIfGameOver()
    {
        if (gameOver && !restart)
        {
            restart = true;
            #if UNITY_ANDROID
            restartButton.SetActive(true);
            #else
            restartText.enabled = true;
            #endif
        }
    }
}
