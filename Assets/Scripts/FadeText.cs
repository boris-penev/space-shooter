﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeText : MonoBehaviour
{
    public float fadeDuration, targetAlpha;
    [TextArea(4, 12)]
    public string contentAndroid, contentDesktop;
    public Text text;

    // Use this for initialization
    void Start()
    {
        #if UNITY_ANDROID
        text.text = contentAndroid;
        #else
        text.text = contentDesktop;
        #endif
        text.CrossFadeAlpha(targetAlpha, fadeDuration, false);
    }
}
