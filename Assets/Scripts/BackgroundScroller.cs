﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroller : MonoBehaviour
{
    public float initialSpeed, maxAcceleration, targetSpeed;
    private float previousPosition, scrollSpeed, tileSizeZ;
    private Vector3 startPosition;

    // Use this for initialization
    void Start()
    {
        startPosition = transform.position;
        tileSizeZ = transform.localScale.y;
        scrollSpeed = initialSpeed;
        previousPosition = tileSizeZ;
    }
	
    // Update is called once per frame
    void Update()
    {
        scrollSpeed = Mathf.MoveTowards(scrollSpeed, targetSpeed,
            maxAcceleration * Mathf.Abs(scrollSpeed));
        float newPosition = previousPosition + Time.deltaTime * scrollSpeed;
        if (newPosition < 0)
        {
            newPosition += tileSizeZ;
        }
        else if (newPosition > tileSizeZ)
        {
            newPosition -= tileSizeZ;
        }
        transform.position = startPosition + Vector3.forward * newPosition;
        previousPosition = newPosition;
    }
}
