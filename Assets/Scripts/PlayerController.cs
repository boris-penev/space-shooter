﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary
{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour
{
    public float fireDelta;
    public float speed;
    public float tilt;
    public Boundary boundary;
    public GameObject shot;
    public SimpleTouchAreaButton areaButton;
    public SimpleTouchPad touchPad;
    public Transform shotSpawn;
    private float nextFire;
    private float myTime = 0.0F;
    private AudioSource audioSource;
    private Quaternion calibrationQuaternion;
    private Rigidbody rigidBody;

    // Use this for initialization
    void Start()
    {
        nextFire = fireDelta;
        audioSource = GetComponent<AudioSource>();
        rigidBody = GetComponent<Rigidbody>();
        #if UNITY_ANDROID
        CalibrateAccelerometer();
        #endif
    }

    void Update()
    {
        myTime = myTime + Time.deltaTime;
        if (FireTrigger() && myTime > nextFire)
        {
            nextFire = myTime + fireDelta;
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
            audioSource.Play();
            nextFire = nextFire - myTime;
            myTime = 0.0F;
        }
    }

    void FixedUpdate()
    {
        #if UNITY_ANDROID
        Vector2 direction = touchPad.GetDirection();
        Vector3 movement = new Vector3(direction.x, 0.0f, direction.y);
        #else
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        #endif
        rigidBody.velocity = movement * speed;
        rigidBody.position = new Vector3(
            Mathf.Clamp(rigidBody.position.x, boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp(rigidBody.position.z, boundary.zMin, boundary.zMax)
        );
        rigidBody.rotation = Quaternion.Euler(0.0f, 0.0f,
            rigidBody.velocity.x * -tilt);
    }

    bool FireTrigger()
    {
        #if UNITY_ANDROID
        return areaButton.CanFire();
        #else
        return Input.GetButton("Fire1") || Input.GetButton("Jump");
        #endif
    }

    #if UNITY_ANDROID
    void CalibrateAccelerometer()
    {
        Vector3 accelerationSnapshot = Input.acceleration;
        Quaternion rotateQuaternenion = Quaternion.FromToRotation(new Vector3(
            0.0f, 0.0f, -1.0f), accelerationSnapshot);
        calibrationQuaternion = Quaternion.Inverse(rotateQuaternenion);
    }

    Vector3 FixAcceleration(Vector3 acceleration)
    {
        return calibrationQuaternion * acceleration;
    }
    #endif
}
